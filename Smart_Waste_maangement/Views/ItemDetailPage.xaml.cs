﻿using Smart_Waste_maangement.ViewModels;
using System.ComponentModel;
using Xamarin.Forms;

namespace Smart_Waste_maangement.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}