﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Smart_Waste_maangement.Models;
using Xamarin.Essentials;



namespace Smart_Waste_maangement.Services
{
    public class ShareImageService
    {
        private const string ApiBaseUrl = "http://localhost:8080/user/registerComplaint";

        public async Task<ComplaintDet> PostDataAsync(ImageWithLocation data)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(data);
                    // Append the endpoint to the base URL
                    string apiUrl = ApiBaseUrl;

                    // Prepare the content to be sent in the request
                    StringContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");

                    // Make the POST request
                    //ttpResponseMessage response = await client.PostAsync(apiUrl, content);
                      HttpResponseMessage response = await client.GetAsync("http://localhost:8080/user/registerComplaint?Area=Kharadi&Latitude=1.1&Longitude=1.1");
                    //HttpResponseMessage response = await client.GetAsync("http://google.com");
                    var head = response.Headers.ToString();
                    // Check if the request was successful (status code 200-299)
                    if (response.IsSuccessStatusCode)
                    {
                        // Read the response content
                        string result = await response.Content.ReadAsStringAsync();
                        var reqid = JObject.Parse(result).ToString(); 
                        var comp = new ComplaintDet
                        {
                            RequestId =  reqid

                        };
                      return comp;
                     
                    }
                    else
                    {
                        // Handle error cases
                        Console.WriteLine($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                        return null;
                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
                return null;
            }
        }
    }
}