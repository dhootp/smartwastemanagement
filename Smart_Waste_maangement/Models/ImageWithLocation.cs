﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Smart_Waste_maangement.Models
{
    public class ImageWithLocation
    {
        public ImageWithLocation() { }

        public string Area { get; set; }

       // public string AreaPicker { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public byte[] Image { get; set; }

        


    }
}