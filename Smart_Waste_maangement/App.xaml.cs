﻿using Smart_Waste_maangement.Services;
//using Smart_Waste_maangement.Views;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Smart_Waste_maangement
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
