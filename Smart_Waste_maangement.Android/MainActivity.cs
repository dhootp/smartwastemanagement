﻿using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using Android;
using Android.Graphics;
//using System.Net;
using Smart_Waste_maangement.Services;
using Smart_Waste_maangement.Droid;
using Smart_Waste_maangement.Models;
using Plugin.Media;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using Plugin.Media.Abstractions;
using System.Threading.Tasks;
//using Xamarin.Forms;
using static System.Net.Mime.MediaTypeNames;
using static Android.Hardware.Camera;
using Xamarin.Forms.Platform.Android;
using System.Collections.Generic;
using Java.Util;
using Android.Content;
using Xamarin.Forms.PlatformConfiguration;




namespace Smart_Waste_maangement.Droid
{
    [Activity(Label = "Smart_Waste_maangement", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize )]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

        Android.Widget.Button captureButton;
        Android.Widget.Button uploadButton;
        Android.Widget.Button submitButton;
        ImageView thisImageView;
        EditText requestId;
        Spinner areaPicker;
        TextView myLabel;
        TextView complaintLabel;


        readonly string[] permissionGroup =
        {
            Manifest.Permission.ReadExternalStorage,
            Manifest.Permission.WriteExternalStorage,
            Manifest.Permission.Camera,
            Manifest.Permission.AccessFineLocation,
            Manifest.Permission.AccessCoarseLocation,
            Manifest.Permission.AccessNetworkState
        };

        ImageWithLocation imgWithLoc;



        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_main);
     
            captureButton = (Android.Widget.Button)FindViewById(Resource.Id.captureButton);
            uploadButton = (Android.Widget.Button)FindViewById(Resource.Id.uploadButton);
            submitButton = (Android.Widget.Button)FindViewById(Resource.Id.submitButton);
            thisImageView = (ImageView)FindViewById(Resource.Id.thisImageView);
            requestId = (EditText)FindViewById(Resource.Id.requestId);
            areaPicker = (Spinner)FindViewById(Resource.Id.areaPicker);
            myLabel = (TextView)FindViewById(Resource.Id.myLabel);
            complaintLabel = (TextView)FindViewById(Resource.Id.complaintLabel);


            captureButton.Click += async (sender, e) => await TakePhotoAsync();
           // areaName.TextChanged += async (sender, e) => await HandleAreaText(areaName.Text);

            var pickerList = new List<string>
                {
                "Kharadi",
                "Khadki",
                "Wagholi",
                "Sangamwadi",
                "Viman nagar",
                "Wadgaon",
                "Vishrantwadi"
                 };
            

            var pickerAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleSpinnerItem, pickerList);
            areaPicker.Adapter = pickerAdapter;
            
            uploadButton.Click += async (sender, e) => await UploadImageWithGeoLocationAsync();
            submitButton.Click += async (sender, e) => await SubmitImageWithGeoLocationAsync();

            RequestPermissions(permissionGroup, 0);
            
        }

        
        private async Task TakePhotoAsync()
        {
            await CrossMedia.Current.Initialize();

            var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
            {
                PhotoSize = PhotoSize.Medium,
                CompressionQuality = 40,
                Name = "I" + DateTime.Now.ToString("yyMMdd_HHmmss") + ".jpg",
                Directory = "sample"
            });

            if (file == null)
            {
                return;
            }

            byte[] imageArray = System.IO.File.ReadAllBytes(file.Path);
            Bitmap bitmap = BitmapFactory.DecodeByteArray(imageArray, 0, imageArray.Length);
            thisImageView.SetImageBitmap(bitmap);

        }  

        // Add the photo to the gallery

        private async Task UploadImageWithGeoLocationAsync()
        {
            await CrossMedia.Current.Initialize();

            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                Toast.MakeText(this, "Upload not supported on this device", ToastLength.Short).Show();
                return;
            }

            var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
            {
                PhotoSize = PhotoSize.Full,
                CompressionQuality = 40
            });

            if (file == null)
            {
                return;
            }

            
            byte[] imageArray = System.IO.File.ReadAllBytes(file.Path);
            // Continue with  image upload logic
            var location = await GetLocationAsync();
            var selectedValue = areaPicker.SelectedItem.ToString();
           // var svc = new ShareImageService();
             imgWithLoc = new ImageWithLocation
            {
                Image = imageArray,
                Longitude = location.Longitude,
                Latitude = location.Latitude,
                Area = selectedValue
            };
            /*
            var compDet = await svc.PostDataAsync("", imgWithLoc);

            Toast.MakeText(this, "Photo uploaded successfully with location latitude and area:" + location.Latitude + " Longitude: " + location.Longitude, ToastLength.Short).Show();


            FindViewById<EditText>(Resource.Id.requestId).Text = compDet.RequestId;
            */
        }

        private async Task SubmitImageWithGeoLocationAsync()
        {
            await CrossMedia.Current.Initialize();

            var svc = new ShareImageService();
            var compDet = await svc.PostDataAsync(imgWithLoc);

           // Toast.MakeText(this, "Photo uploaded successfully with location latitude and area:" + location.Latitude + " Longitude: " + location.Longitude, ToastLength.Short).Show();


            FindViewById<EditText>(Resource.Id.requestId).Text = compDet.RequestId;

        }

        private async Task<Plugin.Geolocator.Abstractions.Position> GetLocationAsync()
        {
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;

                var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10));

                return position;
            }
            catch (Exception ex)
            {
                // Handle exceptions, e.g., if location services are disabled
                Console.WriteLine($"Error getting location: {ex.Message}");
                return null;
            }
        }




        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Plugin.Permissions.PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}