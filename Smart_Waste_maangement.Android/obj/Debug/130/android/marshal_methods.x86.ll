; ModuleID = 'obj\Debug\130\android\marshal_methods.x86.ll'
source_filename = "obj\Debug\130\android\marshal_methods.x86.ll"
target datalayout = "e-m:e-p:32:32-p270:32:32-p271:32:32-p272:64:64-f64:32:64-f80:32-n8:16:32-S128"
target triple = "i686-unknown-linux-android"


%struct.MonoImage = type opaque

%struct.MonoClass = type opaque

%struct.MarshalMethodsManagedClass = type {
	i32,; uint32_t token
	%struct.MonoClass*; MonoClass* klass
}

%struct.MarshalMethodName = type {
	i64,; uint64_t id
	i8*; char* name
}

%class._JNIEnv = type opaque

%class._jobject = type {
	i8; uint8_t b
}

%class._jclass = type {
	i8; uint8_t b
}

%class._jstring = type {
	i8; uint8_t b
}

%class._jthrowable = type {
	i8; uint8_t b
}

%class._jarray = type {
	i8; uint8_t b
}

%class._jobjectArray = type {
	i8; uint8_t b
}

%class._jbooleanArray = type {
	i8; uint8_t b
}

%class._jbyteArray = type {
	i8; uint8_t b
}

%class._jcharArray = type {
	i8; uint8_t b
}

%class._jshortArray = type {
	i8; uint8_t b
}

%class._jintArray = type {
	i8; uint8_t b
}

%class._jlongArray = type {
	i8; uint8_t b
}

%class._jfloatArray = type {
	i8; uint8_t b
}

%class._jdoubleArray = type {
	i8; uint8_t b
}

; assembly_image_cache
@assembly_image_cache = local_unnamed_addr global [0 x %struct.MonoImage*] zeroinitializer, align 4
; Each entry maps hash of an assembly name to an index into the `assembly_image_cache` array
@assembly_image_cache_hashes = local_unnamed_addr constant [248 x i32] [
	i32 32687329, ; 0: Xamarin.AndroidX.Lifecycle.Runtime => 0x1f2c4e1 => 83
	i32 34715100, ; 1: Xamarin.Google.Guava.ListenableFuture.dll => 0x211b5dc => 112
	i32 39109920, ; 2: Newtonsoft.Json.dll => 0x254c520 => 7
	i32 39852199, ; 3: Plugin.Permissions => 0x26018a7 => 11
	i32 57263871, ; 4: Xamarin.Forms.Core.dll => 0x369c6ff => 107
	i32 57967248, ; 5: Xamarin.Android.Support.VersionedParcelable.dll => 0x3748290 => 52
	i32 101534019, ; 6: Xamarin.AndroidX.SlidingPaneLayout => 0x60d4943 => 97
	i32 120558881, ; 7: Xamarin.AndroidX.SlidingPaneLayout.dll => 0x72f9521 => 97
	i32 160529393, ; 8: Xamarin.Android.Arch.Core.Common => 0x9917bf1 => 20
	i32 165246403, ; 9: Xamarin.AndroidX.Collection.dll => 0x9d975c3 => 64
	i32 166922606, ; 10: Xamarin.Android.Support.Compat.dll => 0x9f3096e => 31
	i32 182336117, ; 11: Xamarin.AndroidX.SwipeRefreshLayout.dll => 0xade3a75 => 98
	i32 201930040, ; 12: Xamarin.Android.Arch.Core.Runtime => 0xc093538 => 21
	i32 209399409, ; 13: Xamarin.AndroidX.Browser.dll => 0xc7b2e71 => 62
	i32 230216969, ; 14: Xamarin.AndroidX.Legacy.Support.Core.Utils.dll => 0xdb8d509 => 78
	i32 232815796, ; 15: System.Web.Services => 0xde07cb4 => 122
	i32 261689757, ; 16: Xamarin.AndroidX.ConstraintLayout.dll => 0xf99119d => 67
	i32 278686392, ; 17: Xamarin.AndroidX.Lifecycle.LiveData.dll => 0x109c6ab8 => 82
	i32 280482487, ; 18: Xamarin.AndroidX.Interpolator => 0x10b7d2b7 => 76
	i32 293914992, ; 19: Xamarin.Android.Support.Transition => 0x1184c970 => 47
	i32 318968648, ; 20: Xamarin.AndroidX.Activity.dll => 0x13031348 => 54
	i32 321597661, ; 21: System.Numerics => 0x132b30dd => 15
	i32 342366114, ; 22: Xamarin.AndroidX.Lifecycle.Common => 0x146817a2 => 80
	i32 388313361, ; 23: Xamarin.Android.Support.Animated.Vector.Drawable => 0x17253111 => 27
	i32 389971796, ; 24: Xamarin.Android.Support.Core.UI => 0x173e7f54 => 33
	i32 441335492, ; 25: Xamarin.AndroidX.ConstraintLayout.Core => 0x1a4e3ec4 => 66
	i32 442521989, ; 26: Xamarin.Essentials => 0x1a605985 => 106
	i32 450948140, ; 27: Xamarin.AndroidX.Fragment.dll => 0x1ae0ec2c => 75
	i32 465846621, ; 28: mscorlib => 0x1bc4415d => 6
	i32 469710990, ; 29: System.dll => 0x1bff388e => 14
	i32 476646585, ; 30: Xamarin.AndroidX.Interpolator.dll => 0x1c690cb9 => 76
	i32 486930444, ; 31: Xamarin.AndroidX.LocalBroadcastManager.dll => 0x1d05f80c => 87
	i32 514659665, ; 32: Xamarin.Android.Support.Compat => 0x1ead1551 => 31
	i32 524864063, ; 33: Xamarin.Android.Support.Print => 0x1f48ca3f => 44
	i32 526420162, ; 34: System.Transactions.dll => 0x1f6088c2 => 117
	i32 539750087, ; 35: Xamarin.Android.Support.Design => 0x202beec7 => 37
	i32 571524804, ; 36: Xamarin.Android.Support.v7.RecyclerView => 0x2210c6c4 => 50
	i32 605376203, ; 37: System.IO.Compression.FileSystem => 0x24154ecb => 120
	i32 627609679, ; 38: Xamarin.AndroidX.CustomView => 0x2568904f => 71
	i32 663517072, ; 39: Xamarin.AndroidX.VersionedParcelable => 0x278c7790 => 103
	i32 666292255, ; 40: Xamarin.AndroidX.Arch.Core.Common.dll => 0x27b6d01f => 59
	i32 690569205, ; 41: System.Xml.Linq.dll => 0x29293ff5 => 19
	i32 692692150, ; 42: Xamarin.Android.Support.Annotations => 0x2949a4b6 => 28
	i32 775507847, ; 43: System.IO.Compression => 0x2e394f87 => 119
	i32 801787702, ; 44: Xamarin.Android.Support.Interpolator => 0x2fca4f36 => 41
	i32 809851609, ; 45: System.Drawing.Common.dll => 0x30455ad9 => 114
	i32 843511501, ; 46: Xamarin.AndroidX.Print => 0x3246f6cd => 94
	i32 916714535, ; 47: Xamarin.Android.Support.Print.dll => 0x36a3f427 => 44
	i32 928116545, ; 48: Xamarin.Google.Guava.ListenableFuture => 0x3751ef41 => 112
	i32 955402788, ; 49: Newtonsoft.Json => 0x38f24a24 => 7
	i32 957807352, ; 50: Plugin.CurrentActivity => 0x3916faf8 => 8
	i32 967690846, ; 51: Xamarin.AndroidX.Lifecycle.Common.dll => 0x39adca5e => 80
	i32 974778368, ; 52: FormsViewGroup.dll => 0x3a19f000 => 3
	i32 987342438, ; 53: Xamarin.Android.Arch.Lifecycle.LiveData.dll => 0x3ad9a666 => 24
	i32 1012816738, ; 54: Xamarin.AndroidX.SavedState.dll => 0x3c5e5b62 => 96
	i32 1035644815, ; 55: Xamarin.AndroidX.AppCompat => 0x3dbaaf8f => 58
	i32 1042160112, ; 56: Xamarin.Forms.Platform.dll => 0x3e1e19f0 => 109
	i32 1052210849, ; 57: Xamarin.AndroidX.Lifecycle.ViewModel.dll => 0x3eb776a1 => 84
	i32 1098167829, ; 58: Xamarin.Android.Arch.Lifecycle.ViewModel => 0x4174b615 => 26
	i32 1098259244, ; 59: System => 0x41761b2c => 14
	i32 1104002344, ; 60: Plugin.Media => 0x41cdbd28 => 10
	i32 1137654822, ; 61: Plugin.Permissions.dll => 0x43cf3c26 => 11
	i32 1156335212, ; 62: Smart_Waste_maangement.dll => 0x44ec466c => 12
	i32 1175144683, ; 63: Xamarin.AndroidX.VectorDrawable.Animated => 0x460b48eb => 101
	i32 1178241025, ; 64: Xamarin.AndroidX.Navigation.Runtime.dll => 0x463a8801 => 91
	i32 1204270330, ; 65: Xamarin.AndroidX.Arch.Core.Common => 0x47c7b4fa => 59
	i32 1267360935, ; 66: Xamarin.AndroidX.VectorDrawable => 0x4b8a64a7 => 102
	i32 1282958517, ; 67: Plugin.Geolocator => 0x4c7864b5 => 9
	i32 1292763917, ; 68: Xamarin.Android.Support.CursorAdapter.dll => 0x4d0e030d => 35
	i32 1293217323, ; 69: Xamarin.AndroidX.DrawerLayout.dll => 0x4d14ee2b => 73
	i32 1297413738, ; 70: Xamarin.Android.Arch.Lifecycle.LiveData.Core => 0x4d54f66a => 23
	i32 1359785034, ; 71: Xamarin.Android.Support.Design.dll => 0x510cac4a => 37
	i32 1365406463, ; 72: System.ServiceModel.Internals.dll => 0x516272ff => 113
	i32 1376866003, ; 73: Xamarin.AndroidX.SavedState => 0x52114ed3 => 96
	i32 1395857551, ; 74: Xamarin.AndroidX.Media.dll => 0x5333188f => 88
	i32 1406073936, ; 75: Xamarin.AndroidX.CoordinatorLayout => 0x53cefc50 => 68
	i32 1445445088, ; 76: Xamarin.Android.Support.Fragment => 0x5627bde0 => 40
	i32 1460219004, ; 77: Xamarin.Forms.Xaml => 0x57092c7c => 110
	i32 1462112819, ; 78: System.IO.Compression.dll => 0x57261233 => 119
	i32 1469204771, ; 79: Xamarin.AndroidX.AppCompat.AppCompatResources => 0x57924923 => 57
	i32 1574652163, ; 80: Xamarin.Android.Support.Core.Utils.dll => 0x5ddb4903 => 34
	i32 1582372066, ; 81: Xamarin.AndroidX.DocumentFile.dll => 0x5e5114e2 => 72
	i32 1587447679, ; 82: Xamarin.Android.Arch.Core.Common.dll => 0x5e9e877f => 20
	i32 1592978981, ; 83: System.Runtime.Serialization.dll => 0x5ef2ee25 => 2
	i32 1622152042, ; 84: Xamarin.AndroidX.Loader.dll => 0x60b0136a => 86
	i32 1624863272, ; 85: Xamarin.AndroidX.ViewPager2 => 0x60d97228 => 105
	i32 1636350590, ; 86: Xamarin.AndroidX.CursorAdapter => 0x6188ba7e => 70
	i32 1639515021, ; 87: System.Net.Http.dll => 0x61b9038d => 1
	i32 1657153582, ; 88: System.Runtime => 0x62c6282e => 17
	i32 1658241508, ; 89: Xamarin.AndroidX.Tracing.Tracing.dll => 0x62d6c1e4 => 99
	i32 1658251792, ; 90: Xamarin.Google.Android.Material.dll => 0x62d6ea10 => 111
	i32 1662014763, ; 91: Xamarin.Android.Support.Vector.Drawable => 0x6310552b => 51
	i32 1670060433, ; 92: Xamarin.AndroidX.ConstraintLayout => 0x638b1991 => 67
	i32 1729485958, ; 93: Xamarin.AndroidX.CardView.dll => 0x6715dc86 => 63
	i32 1766324549, ; 94: Xamarin.AndroidX.SwipeRefreshLayout => 0x6947f945 => 98
	i32 1776026572, ; 95: System.Core.dll => 0x69dc03cc => 13
	i32 1788241197, ; 96: Xamarin.AndroidX.Fragment => 0x6a96652d => 75
	i32 1808609942, ; 97: Xamarin.AndroidX.Loader => 0x6bcd3296 => 86
	i32 1813201214, ; 98: Xamarin.Google.Android.Material => 0x6c13413e => 111
	i32 1818569960, ; 99: Xamarin.AndroidX.Navigation.UI.dll => 0x6c652ce8 => 92
	i32 1866717392, ; 100: Xamarin.Android.Support.Interpolator.dll => 0x6f43d8d0 => 41
	i32 1867746548, ; 101: Xamarin.Essentials.dll => 0x6f538cf4 => 106
	i32 1877418711, ; 102: Xamarin.Android.Support.v7.RecyclerView.dll => 0x6fe722d7 => 50
	i32 1878053835, ; 103: Xamarin.Forms.Xaml.dll => 0x6ff0d3cb => 110
	i32 1885316902, ; 104: Xamarin.AndroidX.Arch.Core.Runtime.dll => 0x705fa726 => 60
	i32 1916660109, ; 105: Xamarin.Android.Arch.Lifecycle.ViewModel.dll => 0x723de98d => 26
	i32 1919157823, ; 106: Xamarin.AndroidX.MultiDex.dll => 0x7264063f => 89
	i32 2019465201, ; 107: Xamarin.AndroidX.Lifecycle.ViewModel => 0x785e97f1 => 84
	i32 2037417872, ; 108: Xamarin.Android.Support.ViewPager => 0x79708790 => 53
	i32 2044222327, ; 109: Xamarin.Android.Support.Loader => 0x79d85b77 => 42
	i32 2048185678, ; 110: Plugin.Media.dll => 0x7a14d54e => 10
	i32 2055257422, ; 111: Xamarin.AndroidX.Lifecycle.LiveData.Core.dll => 0x7a80bd4e => 81
	i32 2079903147, ; 112: System.Runtime.dll => 0x7bf8cdab => 17
	i32 2090596640, ; 113: System.Numerics.Vectors => 0x7c9bf920 => 16
	i32 2097448633, ; 114: Xamarin.AndroidX.Legacy.Support.Core.UI => 0x7d0486b9 => 77
	i32 2126786730, ; 115: Xamarin.Forms.Platform.Android => 0x7ec430aa => 108
	i32 2139458754, ; 116: Xamarin.Android.Support.DrawerLayout => 0x7f858cc2 => 39
	i32 2166116741, ; 117: Xamarin.Android.Support.Core.Utils => 0x811c5185 => 34
	i32 2196165013, ; 118: Xamarin.Android.Support.VersionedParcelable => 0x82e6d195 => 52
	i32 2201231467, ; 119: System.Net.Http => 0x8334206b => 1
	i32 2217644978, ; 120: Xamarin.AndroidX.VectorDrawable.Animated.dll => 0x842e93b2 => 101
	i32 2244775296, ; 121: Xamarin.AndroidX.LocalBroadcastManager => 0x85cc8d80 => 87
	i32 2253105697, ; 122: Smart_Waste_maangement.Android => 0x864baa21 => 0
	i32 2256548716, ; 123: Xamarin.AndroidX.MultiDex => 0x8680336c => 89
	i32 2261435625, ; 124: Xamarin.AndroidX.Legacy.Support.V4.dll => 0x86cac4e9 => 79
	i32 2279755925, ; 125: Xamarin.AndroidX.RecyclerView.dll => 0x87e25095 => 95
	i32 2315684594, ; 126: Xamarin.AndroidX.Annotation.dll => 0x8a068af2 => 55
	i32 2330457430, ; 127: Xamarin.Android.Support.Core.UI.dll => 0x8ae7f556 => 33
	i32 2330986192, ; 128: Xamarin.Android.Support.SlidingPaneLayout.dll => 0x8af006d0 => 45
	i32 2373288475, ; 129: Xamarin.Android.Support.Fragment.dll => 0x8d75821b => 40
	i32 2409053734, ; 130: Xamarin.AndroidX.Preference.dll => 0x8f973e26 => 93
	i32 2440966767, ; 131: Xamarin.Android.Support.Loader.dll => 0x917e326f => 42
	i32 2465532216, ; 132: Xamarin.AndroidX.ConstraintLayout.Core.dll => 0x92f50938 => 66
	i32 2471841756, ; 133: netstandard.dll => 0x93554fdc => 115
	i32 2475788418, ; 134: Java.Interop.dll => 0x93918882 => 4
	i32 2487632829, ; 135: Xamarin.Android.Support.DocumentFile => 0x944643bd => 38
	i32 2501346920, ; 136: System.Data.DataSetExtensions => 0x95178668 => 118
	i32 2505896520, ; 137: Xamarin.AndroidX.Lifecycle.Runtime.dll => 0x955cf248 => 83
	i32 2581819634, ; 138: Xamarin.AndroidX.VectorDrawable.dll => 0x99e370f2 => 102
	i32 2620871830, ; 139: Xamarin.AndroidX.CursorAdapter.dll => 0x9c375496 => 70
	i32 2624644809, ; 140: Xamarin.AndroidX.DynamicAnimation => 0x9c70e6c9 => 74
	i32 2633051222, ; 141: Xamarin.AndroidX.Lifecycle.LiveData => 0x9cf12c56 => 82
	i32 2698266930, ; 142: Xamarin.Android.Arch.Lifecycle.LiveData => 0xa0d44932 => 24
	i32 2701096212, ; 143: Xamarin.AndroidX.Tracing.Tracing => 0xa0ff7514 => 99
	i32 2732626843, ; 144: Xamarin.AndroidX.Activity => 0xa2e0939b => 54
	i32 2737747696, ; 145: Xamarin.AndroidX.AppCompat.AppCompatResources.dll => 0xa32eb6f0 => 57
	i32 2751899777, ; 146: Xamarin.Android.Support.Collections => 0xa406a881 => 30
	i32 2766581644, ; 147: Xamarin.Forms.Core => 0xa4e6af8c => 107
	i32 2778768386, ; 148: Xamarin.AndroidX.ViewPager.dll => 0xa5a0a402 => 104
	i32 2788639665, ; 149: Xamarin.Android.Support.LocalBroadcastManager => 0xa63743b1 => 43
	i32 2788775637, ; 150: Xamarin.Android.Support.SwipeRefreshLayout.dll => 0xa63956d5 => 46
	i32 2806986428, ; 151: Plugin.CurrentActivity.dll => 0xa74f36bc => 8
	i32 2810250172, ; 152: Xamarin.AndroidX.CoordinatorLayout.dll => 0xa78103bc => 68
	i32 2819470561, ; 153: System.Xml.dll => 0xa80db4e1 => 18
	i32 2853208004, ; 154: Xamarin.AndroidX.ViewPager => 0xaa107fc4 => 104
	i32 2855708567, ; 155: Xamarin.AndroidX.Transition => 0xaa36a797 => 100
	i32 2876493027, ; 156: Xamarin.Android.Support.SwipeRefreshLayout => 0xab73cce3 => 46
	i32 2893257763, ; 157: Xamarin.Android.Arch.Core.Runtime.dll => 0xac739c23 => 21
	i32 2903344695, ; 158: System.ComponentModel.Composition => 0xad0d8637 => 121
	i32 2905242038, ; 159: mscorlib.dll => 0xad2a79b6 => 6
	i32 2916838712, ; 160: Xamarin.AndroidX.ViewPager2.dll => 0xaddb6d38 => 105
	i32 2919462931, ; 161: System.Numerics.Vectors.dll => 0xae037813 => 16
	i32 2921128767, ; 162: Xamarin.AndroidX.Annotation.Experimental.dll => 0xae1ce33f => 56
	i32 2921692953, ; 163: Xamarin.Android.Support.CustomView.dll => 0xae257f19 => 36
	i32 2922925221, ; 164: Xamarin.Android.Support.Vector.Drawable.dll => 0xae384ca5 => 51
	i32 2978675010, ; 165: Xamarin.AndroidX.DrawerLayout => 0xb18af942 => 73
	i32 3024354802, ; 166: Xamarin.AndroidX.Legacy.Support.Core.Utils => 0xb443fdf2 => 78
	i32 3044182254, ; 167: FormsViewGroup => 0xb57288ee => 3
	i32 3056250942, ; 168: Xamarin.Android.Support.AsyncLayoutInflater.dll => 0xb62ab03e => 29
	i32 3057625584, ; 169: Xamarin.AndroidX.Navigation.Common => 0xb63fa9f0 => 90
	i32 3068715062, ; 170: Xamarin.Android.Arch.Lifecycle.Common => 0xb6e8e036 => 22
	i32 3111772706, ; 171: System.Runtime.Serialization => 0xb979e222 => 2
	i32 3126016514, ; 172: Plugin.Geolocator.dll => 0xba533a02 => 9
	i32 3204380047, ; 173: System.Data.dll => 0xbefef58f => 116
	i32 3204912593, ; 174: Xamarin.Android.Support.AsyncLayoutInflater => 0xbf0715d1 => 29
	i32 3211777861, ; 175: Xamarin.AndroidX.DocumentFile => 0xbf6fd745 => 72
	i32 3233339011, ; 176: Xamarin.Android.Arch.Lifecycle.LiveData.Core.dll => 0xc0b8d683 => 23
	i32 3247949154, ; 177: Mono.Security => 0xc197c562 => 123
	i32 3258312781, ; 178: Xamarin.AndroidX.CardView => 0xc235e84d => 63
	i32 3267021929, ; 179: Xamarin.AndroidX.AsyncLayoutInflater => 0xc2bacc69 => 61
	i32 3296380511, ; 180: Xamarin.Android.Support.Collections.dll => 0xc47ac65f => 30
	i32 3317135071, ; 181: Xamarin.AndroidX.CustomView.dll => 0xc5b776df => 71
	i32 3317144872, ; 182: System.Data => 0xc5b79d28 => 116
	i32 3321585405, ; 183: Xamarin.Android.Support.DocumentFile.dll => 0xc5fb5efd => 38
	i32 3340431453, ; 184: Xamarin.AndroidX.Arch.Core.Runtime => 0xc71af05d => 60
	i32 3346324047, ; 185: Xamarin.AndroidX.Navigation.Runtime => 0xc774da4f => 91
	i32 3352662376, ; 186: Xamarin.Android.Support.CoordinaterLayout => 0xc7d59168 => 32
	i32 3353484488, ; 187: Xamarin.AndroidX.Legacy.Support.Core.UI.dll => 0xc7e21cc8 => 77
	i32 3357663996, ; 188: Xamarin.Android.Support.CursorAdapter => 0xc821e2fc => 35
	i32 3362522851, ; 189: Xamarin.AndroidX.Core => 0xc86c06e3 => 69
	i32 3366347497, ; 190: Java.Interop => 0xc8a662e9 => 4
	i32 3374999561, ; 191: Xamarin.AndroidX.RecyclerView => 0xc92a6809 => 95
	i32 3404865022, ; 192: System.ServiceModel.Internals => 0xcaf21dfe => 113
	i32 3416034496, ; 193: Smart_Waste_maangement => 0xcb9c8cc0 => 12
	i32 3429136800, ; 194: System.Xml => 0xcc6479a0 => 18
	i32 3430777524, ; 195: netstandard => 0xcc7d82b4 => 115
	i32 3439690031, ; 196: Xamarin.Android.Support.Annotations.dll => 0xcd05812f => 28
	i32 3441283291, ; 197: Xamarin.AndroidX.DynamicAnimation.dll => 0xcd1dd0db => 74
	i32 3476120550, ; 198: Mono.Android => 0xcf3163e6 => 5
	i32 3486566296, ; 199: System.Transactions => 0xcfd0c798 => 117
	i32 3493954962, ; 200: Xamarin.AndroidX.Concurrent.Futures.dll => 0xd0418592 => 65
	i32 3498942916, ; 201: Xamarin.Android.Support.v7.CardView.dll => 0xd08da1c4 => 49
	i32 3501239056, ; 202: Xamarin.AndroidX.AsyncLayoutInflater.dll => 0xd0b0ab10 => 61
	i32 3509114376, ; 203: System.Xml.Linq => 0xd128d608 => 19
	i32 3536029504, ; 204: Xamarin.Forms.Platform.Android.dll => 0xd2c38740 => 108
	i32 3547625832, ; 205: Xamarin.Android.Support.SlidingPaneLayout => 0xd3747968 => 45
	i32 3567349600, ; 206: System.ComponentModel.Composition.dll => 0xd4a16f60 => 121
	i32 3618140916, ; 207: Xamarin.AndroidX.Preference => 0xd7a872f4 => 93
	i32 3627220390, ; 208: Xamarin.AndroidX.Print.dll => 0xd832fda6 => 94
	i32 3632359727, ; 209: Xamarin.Forms.Platform => 0xd881692f => 109
	i32 3633644679, ; 210: Xamarin.AndroidX.Annotation.Experimental => 0xd8950487 => 56
	i32 3641409426, ; 211: Smart_Waste_maangement.Android.dll => 0xd90b7f92 => 0
	i32 3641597786, ; 212: Xamarin.AndroidX.Lifecycle.LiveData.Core => 0xd90e5f5a => 81
	i32 3664423555, ; 213: Xamarin.Android.Support.ViewPager.dll => 0xda6aaa83 => 53
	i32 3672681054, ; 214: Mono.Android.dll => 0xdae8aa5e => 5
	i32 3676310014, ; 215: System.Web.Services.dll => 0xdb2009fe => 122
	i32 3678221644, ; 216: Xamarin.Android.Support.v7.AppCompat => 0xdb3d354c => 48
	i32 3681174138, ; 217: Xamarin.Android.Arch.Lifecycle.Common.dll => 0xdb6a427a => 22
	i32 3682565725, ; 218: Xamarin.AndroidX.Browser => 0xdb7f7e5d => 62
	i32 3684561358, ; 219: Xamarin.AndroidX.Concurrent.Futures => 0xdb9df1ce => 65
	i32 3689375977, ; 220: System.Drawing.Common => 0xdbe768e9 => 114
	i32 3714038699, ; 221: Xamarin.Android.Support.LocalBroadcastManager.dll => 0xdd5fbbab => 43
	i32 3718463572, ; 222: Xamarin.Android.Support.Animated.Vector.Drawable.dll => 0xdda34054 => 27
	i32 3718780102, ; 223: Xamarin.AndroidX.Annotation => 0xdda814c6 => 55
	i32 3724971120, ; 224: Xamarin.AndroidX.Navigation.Common.dll => 0xde068c70 => 90
	i32 3758932259, ; 225: Xamarin.AndroidX.Legacy.Support.V4 => 0xe00cc123 => 79
	i32 3776062606, ; 226: Xamarin.Android.Support.DrawerLayout.dll => 0xe112248e => 39
	i32 3786282454, ; 227: Xamarin.AndroidX.Collection => 0xe1ae15d6 => 64
	i32 3822602673, ; 228: Xamarin.AndroidX.Media => 0xe3d849b1 => 88
	i32 3829621856, ; 229: System.Numerics.dll => 0xe4436460 => 15
	i32 3832731800, ; 230: Xamarin.Android.Support.CoordinaterLayout.dll => 0xe472d898 => 32
	i32 3862817207, ; 231: Xamarin.Android.Arch.Lifecycle.Runtime.dll => 0xe63de9b7 => 25
	i32 3874897629, ; 232: Xamarin.Android.Arch.Lifecycle.Runtime => 0xe6f63edd => 25
	i32 3883175360, ; 233: Xamarin.Android.Support.v7.AppCompat.dll => 0xe7748dc0 => 48
	i32 3885922214, ; 234: Xamarin.AndroidX.Transition.dll => 0xe79e77a6 => 100
	i32 3896760992, ; 235: Xamarin.AndroidX.Core.dll => 0xe843daa0 => 69
	i32 3920810846, ; 236: System.IO.Compression.FileSystem.dll => 0xe9b2d35e => 120
	i32 3921031405, ; 237: Xamarin.AndroidX.VersionedParcelable.dll => 0xe9b630ed => 103
	i32 3931092270, ; 238: Xamarin.AndroidX.Navigation.UI => 0xea4fb52e => 92
	i32 3945713374, ; 239: System.Data.DataSetExtensions.dll => 0xeb2ecede => 118
	i32 3955647286, ; 240: Xamarin.AndroidX.AppCompat.dll => 0xebc66336 => 58
	i32 4063435586, ; 241: Xamarin.Android.Support.CustomView => 0xf2331b42 => 36
	i32 4105002889, ; 242: Mono.Security.dll => 0xf4ad5f89 => 123
	i32 4151237749, ; 243: System.Core => 0xf76edc75 => 13
	i32 4182413190, ; 244: Xamarin.AndroidX.Lifecycle.ViewModelSavedState.dll => 0xf94a8f86 => 85
	i32 4216993138, ; 245: Xamarin.Android.Support.Transition.dll => 0xfb5a3572 => 47
	i32 4219003402, ; 246: Xamarin.Android.Support.v7.CardView => 0xfb78e20a => 49
	i32 4292120959 ; 247: Xamarin.AndroidX.Lifecycle.ViewModelSavedState => 0xffd4917f => 85
], align 4
@assembly_image_cache_indices = local_unnamed_addr constant [248 x i32] [
	i32 83, i32 112, i32 7, i32 11, i32 107, i32 52, i32 97, i32 97, ; 0..7
	i32 20, i32 64, i32 31, i32 98, i32 21, i32 62, i32 78, i32 122, ; 8..15
	i32 67, i32 82, i32 76, i32 47, i32 54, i32 15, i32 80, i32 27, ; 16..23
	i32 33, i32 66, i32 106, i32 75, i32 6, i32 14, i32 76, i32 87, ; 24..31
	i32 31, i32 44, i32 117, i32 37, i32 50, i32 120, i32 71, i32 103, ; 32..39
	i32 59, i32 19, i32 28, i32 119, i32 41, i32 114, i32 94, i32 44, ; 40..47
	i32 112, i32 7, i32 8, i32 80, i32 3, i32 24, i32 96, i32 58, ; 48..55
	i32 109, i32 84, i32 26, i32 14, i32 10, i32 11, i32 12, i32 101, ; 56..63
	i32 91, i32 59, i32 102, i32 9, i32 35, i32 73, i32 23, i32 37, ; 64..71
	i32 113, i32 96, i32 88, i32 68, i32 40, i32 110, i32 119, i32 57, ; 72..79
	i32 34, i32 72, i32 20, i32 2, i32 86, i32 105, i32 70, i32 1, ; 80..87
	i32 17, i32 99, i32 111, i32 51, i32 67, i32 63, i32 98, i32 13, ; 88..95
	i32 75, i32 86, i32 111, i32 92, i32 41, i32 106, i32 50, i32 110, ; 96..103
	i32 60, i32 26, i32 89, i32 84, i32 53, i32 42, i32 10, i32 81, ; 104..111
	i32 17, i32 16, i32 77, i32 108, i32 39, i32 34, i32 52, i32 1, ; 112..119
	i32 101, i32 87, i32 0, i32 89, i32 79, i32 95, i32 55, i32 33, ; 120..127
	i32 45, i32 40, i32 93, i32 42, i32 66, i32 115, i32 4, i32 38, ; 128..135
	i32 118, i32 83, i32 102, i32 70, i32 74, i32 82, i32 24, i32 99, ; 136..143
	i32 54, i32 57, i32 30, i32 107, i32 104, i32 43, i32 46, i32 8, ; 144..151
	i32 68, i32 18, i32 104, i32 100, i32 46, i32 21, i32 121, i32 6, ; 152..159
	i32 105, i32 16, i32 56, i32 36, i32 51, i32 73, i32 78, i32 3, ; 160..167
	i32 29, i32 90, i32 22, i32 2, i32 9, i32 116, i32 29, i32 72, ; 168..175
	i32 23, i32 123, i32 63, i32 61, i32 30, i32 71, i32 116, i32 38, ; 176..183
	i32 60, i32 91, i32 32, i32 77, i32 35, i32 69, i32 4, i32 95, ; 184..191
	i32 113, i32 12, i32 18, i32 115, i32 28, i32 74, i32 5, i32 117, ; 192..199
	i32 65, i32 49, i32 61, i32 19, i32 108, i32 45, i32 121, i32 93, ; 200..207
	i32 94, i32 109, i32 56, i32 0, i32 81, i32 53, i32 5, i32 122, ; 208..215
	i32 48, i32 22, i32 62, i32 65, i32 114, i32 43, i32 27, i32 55, ; 216..223
	i32 90, i32 79, i32 39, i32 64, i32 88, i32 15, i32 32, i32 25, ; 224..231
	i32 25, i32 48, i32 100, i32 69, i32 120, i32 103, i32 92, i32 118, ; 232..239
	i32 58, i32 36, i32 123, i32 13, i32 85, i32 47, i32 49, i32 85 ; 248..247
], align 4

@marshal_methods_number_of_classes = local_unnamed_addr constant i32 0, align 4

; marshal_methods_class_cache
@marshal_methods_class_cache = global [0 x %struct.MarshalMethodsManagedClass] [
], align 4; end of 'marshal_methods_class_cache' array


@get_function_pointer = internal unnamed_addr global void (i32, i32, i32, i8**)* null, align 4

; Function attributes: "frame-pointer"="none" "min-legal-vector-width"="0" mustprogress nofree norecurse nosync "no-trapping-math"="true" nounwind sspstrong "stack-protector-buffer-size"="8" "stackrealign" "target-cpu"="i686" "target-features"="+cx8,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "tune-cpu"="generic" uwtable willreturn writeonly
define void @xamarin_app_init (void (i32, i32, i32, i8**)* %fn) local_unnamed_addr #0
{
	store void (i32, i32, i32, i8**)* %fn, void (i32, i32, i32, i8**)** @get_function_pointer, align 4
	ret void
}

; Names of classes in which marshal methods reside
@mm_class_names = local_unnamed_addr constant [0 x i8*] zeroinitializer, align 4
@__MarshalMethodName_name.0 = internal constant [1 x i8] c"\00", align 1

; mm_method_names
@mm_method_names = local_unnamed_addr constant [1 x %struct.MarshalMethodName] [
	; 0
	%struct.MarshalMethodName {
		i64 0, ; id 0x0; name: 
		i8* getelementptr inbounds ([1 x i8], [1 x i8]* @__MarshalMethodName_name.0, i32 0, i32 0); name
	}
], align 8; end of 'mm_method_names' array


attributes #0 = { "min-legal-vector-width"="0" mustprogress nofree norecurse nosync "no-trapping-math"="true" nounwind sspstrong "stack-protector-buffer-size"="8" uwtable willreturn writeonly "frame-pointer"="none" "target-cpu"="i686" "target-features"="+cx8,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "tune-cpu"="generic" "stackrealign" }
attributes #1 = { "min-legal-vector-width"="0" mustprogress "no-trapping-math"="true" nounwind sspstrong "stack-protector-buffer-size"="8" uwtable "frame-pointer"="none" "target-cpu"="i686" "target-features"="+cx8,+mmx,+sse,+sse2,+sse3,+ssse3,+x87" "tune-cpu"="generic" "stackrealign" }
attributes #2 = { nounwind }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}
!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{i32 1, !"NumRegisterParameters", i32 0}
!3 = !{!"Xamarin.Android remotes/origin/d17-5 @ 45b0e144f73b2c8747d8b5ec8cbd3b55beca67f0"}
!llvm.linker.options = !{}
