<%@ page import="java.util.Arrays" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Smart Waste Management System </title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f0f0f0;
        }
        header {
            background-color: #4CAF50;
            color: #fff;
            padding: 20px;
            text-align: center;
        }
        footer {
            background-color: #333;
            color: #fff;
            padding: 5px;
            text-align: center;
            position: fixed;
            bottom: 0;
            width: 100%;
        }

        #myPieChart {
            width: 400px; /* Set width */
            height: 400px; /* Set height */
        }
        .tablink {
            background-color: #555;
            color: white;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            font-size: 17px;
            width: 25%;
        }

        .tablink:hover {
            background-color: #777;
        }

            </style>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src=
                    "https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js">
    </script>


</head>
<body onload="initMap()">


    <header>
        <h1 style="color:whitesmoke">Smart Waste Management System</h1>
        <br>
        <a href="/dashboard"><button class="tablink">Back to Dashboard</button></a>
        <br>
    </header>

</header>

<div class="container">
    <h1>Complaint Status Report</h1>
        <!-- Canvas element for the pie chart -->
    <canvas id="myPieChart" ></canvas>

    <script>

        $(document).ready(function(){

            function loadPieData() {
                $.ajax({
                    url: 'http://localhost:8080/report/ViewStatusReport',
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {

                        $('#output').html(JSON.stringify(data));
                        var pieData=Array.of(data.newComplaints, data.inProgress, data.closed);
                        createPieChart(pieData)


                    },
                    error: function(xhr, status, error) {
                        // Handle errors here
                        console.error(error);
                    }
                });
            }

            function loadBarChartData() {
                $.ajax({
                    url: 'http://localhost:8080/report/ViewAreaReport',
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                      createBarChart(data)


                    },
                    error: function(xhr, status, error) {
                        // Handle errors here
                        console.error(error);
                    }
                });
            }
            // Function to create pie chart with data
            function createPieChart(piData) {
                var data = {
                    labels: [ "New", "In-Progress", "Resolved"],
                    datasets: [{
                        data: piData,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.5)',
                            'rgba(54, 162, 235, 0.5)',
                            'rgba(255, 206, 86, 0.5)',
                        ]
                    }]
                };
 var option1={

                      responsive: false,


                };

                var ctx = document.getElementById('myPieChart').getContext('2d');


                var myPieChart = new Chart(ctx, {
                    type: 'pie',
                    data: data,
                    options:option1
                });

            }


            loadPieData();
            loadBarChartData();

        });
    </script>

</div>

<div>
    <h1>Area wise Report </h1>
    <canvas id="barChart" width=400" height="400"></canvas>

    <script>

        function createBarChart(barData) {
            var labels =[];
            var values=[];
            for(let i = 0; i < barData.length; i++) {
                let obj = barData[i];
                labels[i]=obj.areaName;
                values[i]=obj.count;

            }
            var ctx = document.getElementById('barChart').getContext('2d');
            var barChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: labels,

                    datasets: [{
                        label: 'Values',
                        data: values,
                        backgroundColor: 'rgba(54, 162, 235, 0.5)',
                        borderColor: 'rgba(54, 162, 235, 1)',
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }
    </script>
</div>
<div>
    <h1>Geo Location Tag Report for Filled Bins</h1>


<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwLWvPa86Szosbakyv_2ZBQ9Z9Wmv1HJA"></script>

<div  id="map" style="height: 400px;"></div>

    <script>

        function initMap() {

            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 18.5509, lng: 73.9506},
                zoom: 13
            });


            var xhr = new XMLHttpRequest();
            xhr.open('GET', 'http://localhost:8080/report/ViewMapReport', true); // This URL should match the servlet mapping
            xhr.onload = function () {
                if (xhr.status >= 200 && xhr.status < 300) {

                    var coordinates = JSON.parse(xhr.responseText);
                    console.log(coordinates);
                  /*  var locations = [
                        ['Location 1', 18.5555, 73.9505],
                        ['Location 2', 18.5502840, 73.9349803], // Example Location 1
                        ['Location 3', 18.5582421, 73.9536749], // Example Location 2
                        ['Location 3', 18.5508, 73.9391],

                    ];
*/

                    var locations = [
                        ['13201', 18.5502840, 73.9349803],
                        ['14502', 18.5552104, 73.9531399], // Example Location 1
                        ['16703', 18.5582421, 73.9536749], // Example Location 2
                        ['19804', 18.5512508, 73.9506698],
                        ['10705', 18.5436790, 73.9423653],
                        ['14006', 18.5594155, 73.9120501],
                        ['18907', 18.5599046, 73.9130546],
                        ['11108', 18.5591888, 73.9108908],
                        ['65409', 18.5670015, 73.8782972],
                        ['66610', 18.5669410, 73.8783660],
                        ['14111', 18.5670012, 73.8782979],
                        ['62212', 18.5669020, 73.8782140],
                        ['54113', 18.5669020, 73.8782140],
                        ['99914', 18.5512508, 73.9506698],
                        ['65115', 18.5594155, 73.9120501],
                        ['19116', 18.5599046, 73.9130546],
                        ['45217', 18.5599046, 73.9130546],
                        ['89418', 18.5599046, 73.9130546],
                        ['66619', 18.5582421, 73.9536749],
                        ['20120', 18.5512508, 73.9506698]




                    ];
                    for (var i = 0; i < coordinates.length; i++) {
                        var coordinate = coordinates[i];

                        console.log(coordinate.latitude);
                        var marker = new google.maps.Marker({
                            position: {lat: coordinate.latitude, lng: coordinate.longitude},
                            map: map,
                            title: coordinate.complaintId
                        });
                    }


                    for (var i = 0; i < locations.length; i++) {
                        var location = locations[i];
                        var marker = new google.maps.Marker({
                            position: {lat: location[1], lng: location[2]},
                            map: map,
                            title: location[0]
                        });
                    }


                } else {
                    console.error('Failed to fetch coordinates from REST API');
                    setTimeout(initMap, 2000);
                }
            };
            xhr.send();
        }



</script>

<br>

</div>



<%--<footer>
<p>&copy; 2024 Smart Waste Management Application. All rights reserved.</p>
</footer>--%>
</body>
</body>
</html>