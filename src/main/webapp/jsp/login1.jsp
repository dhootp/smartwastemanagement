<!DOCTYPE html>
<html>
<head>
    <title>Load Data from REST API</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function(){
            // Function to make AJAX request to the REST API
            function loadData() {
                $.ajax({
                    url: 'your_rest_api_endpoint',
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        // Handle the response data here
                        console.log(data);
                        // Example: Output response data to a div with id 'output'
                        $('#output').html(JSON.stringify(data));
                    },
                    error: function(xhr, status, error) {
                        // Handle errors here
                        console.error(error);
                    }
                });
            }

            // Call the loadData function when the page loads
            loadData();
        });
    </script>
</head>
<body>
<!-- This div will display the response from the REST API -->
<div id="output"></div>
</body>
</html>


<table>
    <thead>
    <tr>
        <th>Area</th>
        <th>Garbage Collected (tons)</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>Downtown</td>
        <td>25</td>
    </tr>
    <tr>
        <td>Suburb A</td>
        <td>15</td>
    </tr>
    <tr>
        <td>Suburb B</td>
        <td>18</td>
    </tr>
    <tr>
        <td>Industrial Zone</td>
        <td>30</td>
    </tr>
    </tbody>
</table>