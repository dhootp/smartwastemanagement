<html>
<head>
    <title>Smart Waste Management System</title>
</head>
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        background-color: #f0f0f0;
    }
    header {
        background-color: #4CAF50;
        color: #fff;
        padding: 20px;
        text-align: center;
    }
    footer {
        background-color: #333;
        color: #fff;
        padding: 10px;
        text-align: center;
        position: fixed;
        bottom: 0;
        width: 100%;
    }
    .autoResizeImage {
        max-width: 50%;
        height: auto;
        width: 500px;
    }

    /* Style for the table */
    table {
        border-collapse: collapse; /* Collapse the borders into a single border */
        width: 100%; /* Make the table occupy full width */
    }

    /* Style for table header */
    th {
        border-bottom: 2px solid #ddd; /* Add a bottom border with a low thickness */
        font-weight: bold; /* Make the header text bold */
        padding: 8px; /* Add padding to the header cells */
        text-align: left; /* Align header text to the left */
    }

    /* Style for table data */
    td {
        border-bottom: 1px solid #ddd; /* Add a bottom border with a low thickness */
        padding: 8px; /* Add padding to the data cells */
        text-align: left; /* Align data text to the left */
    }

    /* Style for alternate table rows */
    tr:nth-child(even) {
        background-color: #f2f2f2; /* Add a background color to alternate rows for better readability */
    }


    .tablink {
        background-color: #555;
        color: white;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        font-size: 17px;
        width: 25%;
    }

    .tablink:hover {
        background-color: #777;
    }

    /* Style the tab content (and add height:100% for full page content) */
    .tabcontent {
        color: white;
        display: none;
        padding: 100px 20px;
        height: 100%;
    }

</style>
<body>

<header>
    <h1 style="color:whitesmoke">Smart Waste Management System</h1>

</header>

<button class="tablink" onclick="loadNewComplaints(this)" id="defaultOpen">New Complaints</button>
<button class="tablink" onclick="loadAssignedComplaints(this)" id="assignTab">Assigned Complaints</button>
<button class="tablink" onclick="loadClosedComplaints(this)" id="closeTab">Closed Complaints</button>
<a href="/report"><button class="tablink">Report</button></a>


<div id="tableContainer" class="tabcontent">
    <h3>New Complaints</h3>
    <p>Here the list of new complanits ..</p>
</div>

<div id="AssignedContainer" class="tabcontent">
    <h3>Assigned Complaints</h3>
    <p>Here the list of new complanits ..</p>
</div>

<div id="closedContainer" class="tabcontent">
    <h3>Closed Complaints</h3>
    <p>Here the list of new complanits ..</p>
</div>

<div id="ReportContainer" class="tabcontent">
    <h3>Report</h3>

</div>


<script src=
                "https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js">
</script>
<script>

    function generateTable(data, buttonName, functionName) {
        var tableHtml = '<table border="1">';
        tableHtml += '<tr> <th>Complaint Id</th><th>Location</th><th>Image</th><th>Action</th></tr>';
        data.forEach(function(data) {
            tableHtml += '<tr>';
            tableHtml += '<td>' + data.complaintId + '</td>';
            tableHtml += '<td>' + data.area  + '</td>';
            tableHtml += '<td><img class="autoResizeImage" src="data:image/png;base64,'+data.image +'" ></td>';
          if(buttonName!="") {
              tableHtml += '<td><button onclick="' + functionName + '(\'' + data.complaintId + '\')">' + buttonName + '</button></td>';
          }else
          {
              tableHtml += '<td></td>'
          }
            tableHtml += '</tr>';
        });
        tableHtml += '</table>';
        return tableHtml;
    }

    function assignComplaint(complaintId)
    {
        const URL =
            `http://localhost:8080/admin/assignComplaint?complaintId=`+complaintId
        $.ajax({
            url: URL, method: "GET", success: function (result) {

                alert("Assigned complaint Id " + result.complaintId + " to " + result.assignedTo );
              //  $("#posts-container").innerHTML = "";
                var tableContainer = document.getElementById('tableContainer');
                tableContainer.innerHTML = "";
                var defaultOpen = document.getElementById('defaultOpen');
                loadNewComplaints(defaultOpen);
            }
        })

    }

    function closeComplaint(complaintId)
    {
        const URL =
            `http://localhost:8080/admin/updateComplaintStatus?complaintId=`+complaintId + `&status=Closed`
        $.ajax({
            url: URL, method: "GET", success: function (result) {

                alert( "Complaint " + complaintId +" Resolved - Notification Sent!" );
                //  $("#posts-container").innerHTML = "";
                var tableContainer = document.getElementById('AssignedContainer');
                tableContainer.innerHTML = "";
                var assignetTab= document.getElementById("assignTab")
                loadAssignedComplaints(assignetTab);
            }
        })

    }
    // We will write our code here
 /*   $("#get-request").click(() => {*/

    function loadNewComplaints(elmnt){
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }

        document.getElementById("tableContainer").style.display = "block";
      // elmnt.style.backgroundColor = 808080;
        const URL =
            `http://localhost:8080/admin/viewComplaints?status=New`
        $.ajax({
            url: URL, method: "GET", success: function (result) {
                var innerHtlm = generateTable(result, "Assign", "assignComplaint" );
                var tableContainer = document.getElementById('tableContainer');
                tableContainer.innerHTML = innerHtlm;

            }
        })
        }

    function loadAssignedComplaints(elmnt){
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById("AssignedContainer").style.display = "block";
       elmnt.style.backgroundColor = 808080;
        const URL =
            `http://localhost:8080/admin/viewComplaints?status=Assigned`
        $.ajax({
            url: URL, method: "GET", success: function (result) {
                var innerHtlm = generateTable(result, "Close","closeComplaint");
                var tableContainer = document.getElementById('AssignedContainer');
                tableContainer.innerHTML = innerHtlm;
                //$("#posts-container").append(innerHtlm);

            }
        })
    }

    function loadClosedComplaints(elmnt){
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }

        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById("closedContainer").style.display = "block";
        elmnt.style.backgroundColor = 808080;
        const URL =
            `http://localhost:8080/admin/viewComplaints?status=Closed`
        $.ajax({
            url: URL, method: "GET", success: function (result) {
                var innerHtlm = generateTable(result, "","");
                var tableContainer = document.getElementById('closedContainer');
                tableContainer.innerHTML = innerHtlm;
                //$("#posts-container").append(innerHtlm);

            }
        })
    }
    document.getElementById("defaultOpen").click();
     /*   })
    })*/
</script>


</body>

<footer>
    <p>&copy; 2024 Smart Waste Management Application. All rights reserved.</p>
</footer>

</html>
