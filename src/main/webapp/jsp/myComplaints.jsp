<html>
<head>
    <title>Smart Waste Management System</title>
</head>
<style>
    body {
        font-family: Arial, sans-serif;
        margin: 0;
        padding: 0;
        background-color: #f0f0f0;
    }
    header {
        background-color: #4CAF50;
        color: #fff;
        padding: 20px;
        text-align: center;
    }
    footer {
        background-color: #333;
        color: #fff;
        padding: 10px;
        text-align: center;
        position: fixed;
        bottom: 0;
        width: 100%;
    }
    .autoResizeImage {
        max-width: 50%;
        height: auto;
        width: 500px;
    }

    /* Style for the table */
    table {
        border-collapse: collapse; /* Collapse the borders into a single border */
        width: 100%; /* Make the table occupy full width */
    }

    /* Style for table header */
    th {
        border-bottom: 2px solid #ddd; /* Add a bottom border with a low thickness */
        font-weight: bold; /* Make the header text bold */
        padding: 8px; /* Add padding to the header cells */
        text-align: left; /* Align header text to the left */
    }

    /* Style for table data */
    td {
        border-bottom: 1px solid #ddd; /* Add a bottom border with a low thickness */
        padding: 8px; /* Add padding to the data cells */
        text-align: left; /* Align data text to the left */
    }

    /* Style for alternate table rows */
    tr:nth-child(even) {
        background-color: #f2f2f2; /* Add a background color to alternate rows for better readability */
    }


    .tablink {
        background-color: #555;
        color: white;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        font-size: 17px;
        width: 25%;
    }

    .tablink:hover {
        background-color: #777;
    }

    /* Style the tab content (and add height:100% for full page content) */
    .tabcontent {
        color: white;
        display: none;
        padding: 100px 20px;
        height: 100%;
    }

</style>
<body>

<header>
    <h1 style="color:whitesmoke">Smart Waste Management System</h1>

</header>

<footer>
    <p>&copy; 2024 Smart Waste Management Application. All rights reserved.</p>
</footer>
</body>