drop table if exists Complaint;
create table Complaint(
complaint_id VARCHAR(20) not null,
status VARCHAR(20) not null,
image_path VARCHAR(50) not null,
area VARCHAR(20) not null,
latitude FLOAT not null,
longitude FLOAT not null,
assigned_to VARCHAR(20) DEFAULT 'NA');

drop table if exists Officer;
create table Officer(
officer_name VARCHAR(20) not null,
is_idle CHAR(1) DEFAULT 'Y',
area VARCHAR(20) not null);