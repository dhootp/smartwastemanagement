insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('13201', 'New', 'src/main/resources/images/13201/', 'Kharadi', 18.5502840, 73.9349803, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('14502', 'New', 'src/main/resources/images/14502/', 'Kharadi', 18.5552104, 73.9531399, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('16703', 'New', 'src/main/resources/images/16703/', 'Kharadi', 18.5582421, 73.9536749, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('19804', 'New', 'src/main/resources/images/19804/', 'Kharadi', 18.5512508, 73.9506698, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('10705', 'New', 'src/main/resources/images/10705/', 'Kharadi', 18.5436790, 73.9423653, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('14006', 'New', 'src/main/resources/images/14006/', 'Viman nagar', 18.5594155, 73.9120501, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('18907', 'New', 'src/main/resources/images/18907/', 'Viman nagar', 18.5599046, 73.9130546, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('11108', 'New', 'src/main/resources/images/11108/', 'Viman nagar', 18.5591888, 73.9108908, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('65409', 'New', 'src/main/resources/images/65409/', 'Vishrantwadi', 18.5670015, 73.8782972, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('66610', 'New', 'src/main/resources/images/66610/', 'Vishrantwadi', 18.5669410, 73.8783660, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('14111', 'New', 'src/main/resources/images/14111/', 'Vishrantwadi', 18.5670012, 73.8782979, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('62212', 'New', 'src/main/resources/images/62212/', 'Vishrantwadi', 18.5669020, 73.8782140, 'NA');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('54113', 'Assigned', 'src/main/resources/images/54113/', 'Vishrantwadi', 18.5669020, 73.8782140, 'worker7');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('99914', 'Assigned', 'src/main/resources/images/99914/', 'Kharadi', 18.5512508, 73.9506698, 'worker1');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('65115', 'Assigned', 'src/main/resources/images/65115/', 'Viman nagar', 18.5594155, 73.9120501, 'worker5');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('19116', 'Closed', 'src/main/resources/images/19116/', 'Viman nagar', 18.5599046, 73.9130546, 'worker5');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('45217', 'Closed', 'src/main/resources/images/45217/', 'Viman nagar', 18.5599046, 73.9130546, 'worker5');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('89418', 'Closed', 'src/main/resources/images/89418/', 'Viman nagar', 18.5599046, 73.9130546, 'worker5');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('66619', 'Closed', 'src/main/resources/images/66619/', 'Kharadi', 18.5582421, 73.9536749, 'worker1');
insert into complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values ('20120', 'Closed', 'src/main/resources/images/20120/', 'Kharadi', 18.5512508, 73.9506698, 'worker1');





insert into officer (officer_name, is_idle, area) values ('worker1', 'Y', 'Kharadi');
insert into officer (officer_name, is_idle, area) values ('worker2', 'Y', 'Khadki');
insert into officer (officer_name, is_idle, area) values ('worker3', 'Y', 'Wagholi');
insert into officer (officer_name, is_idle, area) values ('worker4', 'Y', 'Sangamwadi');
insert into officer (officer_name, is_idle, area) values ('worker5', 'Y', 'Viman nagar');
insert into officer (officer_name, is_idle, area) values ('worker6', 'Y', 'Wadgaon');
insert into officer (officer_name, is_idle, area) values ('worker7', 'Y', 'Vishrantwadi');