package com.wit.hackathon.SmartWasteManager.model;

public class StatusReport {
     int newComplaints;
    int inProgress;
    int closed;

    public int getNewComplaints() {
        return newComplaints;
    }

    public void setNewComplaints(int newComplaints) {
        this.newComplaints = newComplaints;
    }

    public int getInProgress() {
        return inProgress;
    }

    public void setInProgress(int inProgress) {
        this.inProgress = inProgress;
    }

    public int getClosed() {
        return closed;
    }

    public void setClosed(int closed) {
        this.closed = closed;
    }
}
