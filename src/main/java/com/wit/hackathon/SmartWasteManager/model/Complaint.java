package com.wit.hackathon.SmartWasteManager.model;

import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

public class Complaint {
    String complaintId;
    ComplaintStatus status = ComplaintStatus.New;
    byte[] image;
    String area;
    float latitude;
    float longitude;
    String assignedTo = "NA";


    public Complaint(String complaintId){
        this.complaintId = complaintId;
    }

    @Override
    public String toString() {
        return "Complaint{" +
                "complaintId='" + complaintId + '\'' +
                ", status=" + status +
                ", image=" + Arrays.toString(image) +
                ", area='" + area + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", assignedTo='" + assignedTo + '\'' +
                '}';
    }

    public String getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(String complaintId) {
        this.complaintId = complaintId;
    }

    public ComplaintStatus getStatus() {
        return status;
    }

    public void setStatus(ComplaintStatus status) {
        this.status = status;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }


    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }
}
