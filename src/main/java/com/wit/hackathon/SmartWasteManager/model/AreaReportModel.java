package com.wit.hackathon.SmartWasteManager.model;

public class AreaReportModel {

    String areaName;


    public AreaReportModel(){}
    public AreaReportModel(String areaName, int count) {
        this.areaName = areaName;
        this.count = count;
    }

    int count;

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
