package com.wit.hackathon.SmartWasteManager.model;

public class MapReport {

    String complaintId;
    double longitude;
    double latitude;

    public MapReport(String complaintId, double longitude, double latitude) {
        this.complaintId = complaintId;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getComplaintId() {
        return complaintId;
    }

    public void setComplaintId(String complaintId) {
        this.complaintId = complaintId;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
