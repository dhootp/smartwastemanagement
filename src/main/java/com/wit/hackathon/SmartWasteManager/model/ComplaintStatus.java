package com.wit.hackathon.SmartWasteManager.model;

public enum ComplaintStatus {
    //TODO officer will set the status to in progress and in review
    New, Assigned, In_Progress, In_Review, Closed
}
