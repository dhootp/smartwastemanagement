package com.wit.hackathon.SmartWasteManager.controller;

import com.wit.hackathon.SmartWasteManager.model.Complaint;
import com.wit.hackathon.SmartWasteManager.model.ComplaintStatus;
import com.wit.hackathon.SmartWasteManager.service.AdminService;
import com.wit.hackathon.SmartWasteManager.service.ImageProcessingService;
import com.wit.hackathon.SmartWasteManager.service.DatabaseConnection;
import com.wit.hackathon.SmartWasteManager.service.LoginService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;


import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

@RestController
public class AdminController {
    @Autowired
    AdminService adminService;

    //used by frontend
    @GetMapping(value = "admin/viewComplaints")
    public ArrayList<Complaint> viewComplaints(
            @RequestParam("status") String status
    ) {
        return adminService.getComplaints(status);
    }

    @GetMapping(value = "admin/assignComplaint", consumes = MediaType.ALL_VALUE)
    public Complaint assignComplaint(
        @RequestParam("complaintId") String complaintId
    ) {
        return adminService.assignComplaint(complaintId);
    }

    @GetMapping(value = "admin/updateComplaintStatus", consumes = MediaType.ALL_VALUE)
    public Complaint closeComplaint(
            @RequestParam("complaintId") String complaintId,
            @RequestParam("status") String status
    ) {
        return adminService.updateComplaintStatus(complaintId, status);
    }
}
