package com.wit.hackathon.SmartWasteManager.controller;

import com.wit.hackathon.SmartWasteManager.model.Complaint;
import com.wit.hackathon.SmartWasteManager.service.ComplaintService;
import com.wit.hackathon.SmartWasteManager.service.ImageProcessingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.awt.*;
import java.io.IOException;

@RestController
public class ComplaintController
{

    @Autowired
    ImageProcessingService imageProcessingService;

    @Autowired
    ComplaintService complaintService;

    @PostMapping(value = "user/registerComplaint", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String registerComplaint(
            @RequestParam("Area") String area,
            @RequestParam("Latitude") String latitude,
            @RequestParam("Longitude") String longitude,
            @RequestParam("Image") MultipartFile garbageImages

    ) throws IOException {

        String complaintNumber= complaintService.generateComplaintNumber();

        String uploadDirectory = "src/main/resources/images/" + complaintNumber;
        String   garbageImagesString = imageProcessingService.saveImageToStorage(uploadDirectory, garbageImages) + ",";
        Complaint complaint = new Complaint(complaintNumber);
        complaint.setArea(area);
        complaint.setLatitude(Float.parseFloat(latitude));
        complaint.setLongitude(Float.parseFloat(longitude));
        if(complaintService.registerComplaint (complaint, uploadDirectory)){
            return "Your complaint number is  " + complaintNumber;
        }
        return "Error registering complaint";
    }


    @GetMapping("/heartBeat")
    public String heartBeat()
    {
        return "ok";
    }
}
