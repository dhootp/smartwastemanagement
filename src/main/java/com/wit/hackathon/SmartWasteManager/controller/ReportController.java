package com.wit.hackathon.SmartWasteManager.controller;

import com.wit.hackathon.SmartWasteManager.model.*;
import com.wit.hackathon.SmartWasteManager.service.DatabaseConnection;
import com.wit.hackathon.SmartWasteManager.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


@RestController
public class ReportController {
    Connection conn = DatabaseConnection.getConnection();
    Statement stmt = null;

    @GetMapping(value = "report/ViewStatusReport")
    public StatusReport viewStatusReport() throws IOException {

        StatusReport sr = new StatusReport();
        try {
            stmt = conn.createStatement();
            String sql = "SELECT status, count(*) as status_count from Complaint group by status";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                if (rs.getString("status").equals(ComplaintStatus.New.name()))
                    sr.setNewComplaints(rs.getInt("status_count"));
                else if (rs.getString("status").equals(ComplaintStatus.Assigned.name()))
                    sr.setInProgress(rs.getInt("status_count"));
                else if (rs.getString("status").equals(ComplaintStatus.Closed.name()))
                    sr.setClosed(rs.getInt("status_count"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sr;
    }

    @GetMapping(value = "report/ViewAreaReport")
    public ArrayList<AreaReportModel> viewAreaReport() throws IOException {

        ArrayList<AreaReportModel> areaStatistic = new ArrayList<>();
        try {
            stmt = conn.createStatement();
            String sql = "SELECT area, count(*) as area_count from Complaint group by area";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                AreaReportModel areaReportModel = new AreaReportModel(rs.getString("area"), rs.getInt("area_count"));
                areaStatistic.add(areaReportModel);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return areaStatistic;
    }

    @GetMapping(value = "report/ViewMapReport")
    public ArrayList<MapReport> viewMapReport() throws IOException {


        ArrayList<MapReport> mapReportList = new ArrayList<>();
        try {
            stmt = conn.createStatement();
            String sql = "SELECT complaint_id, latitude, longitude from Complaint";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                MapReport mapReport = new MapReport(rs.getString("complaint_id"), rs.getFloat("latitude"), rs.getFloat("longitude"));
                mapReportList.add(mapReport);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return mapReportList;


    }
}