package com.wit.hackathon.SmartWasteManager.service;

import com.wit.hackathon.SmartWasteManager.model.Complaint;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

@Service
public class ComplaintService {
    Connection conn = DatabaseConnection.getConnection();
    Statement stmt = null;

    public String generateComplaintNumber()
    {
        //Ideally take the next number from the database
        Random rand = new Random();
        int rand_int1 = rand.nextInt(99999);
        return Integer.toString(rand_int1);
    }

    public boolean registerComplaint(Complaint complaint, String imagePath)
    {
        try {
            stmt = conn.createStatement();
            String sql = "INSERT into Complaint (complaint_id, status, image_path, area, latitude, longitude, assigned_to) values('" + complaint.getComplaintId()
                    + "','" + complaint.getStatus() +"','" + imagePath +"','" + complaint.getArea() + "','" + complaint.getLatitude() + "','" + complaint.getLongitude() + "','" + complaint.getAssignedTo() +"')";
            stmt.execute(sql);
        }catch (SQLException e){
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
