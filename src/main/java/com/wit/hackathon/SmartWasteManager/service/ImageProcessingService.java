package com.wit.hackathon.SmartWasteManager.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@Service
public class ImageProcessingService {


        // Save image in a local directory
        public String saveImageToStorage(String uploadDirectory, MultipartFile imageFile)
                throws IOException {
            Path uploadPath = Path.of(uploadDirectory);
            String uniqueFileName  = uploadPath.getFileName().toString() + ".jpg";
            Path filePath = uploadPath.resolve(uniqueFileName);

            if (!Files.exists(uploadPath)) {
                Files.createDirectories(uploadPath);
            }

            Files.copy(imageFile.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);

            return uniqueFileName;
        }

        // To view an image
        public byte[]

        getImage(String imageDirectory, String imageName) throws IOException {
            Path imagePath = Path.of(imageDirectory, imageName);

            if (Files.exists(imagePath)) {
                byte[] imageBytes = Files.readAllBytes(imagePath);
                return imageBytes;
            } else {
                return null; // Handle missing images
            }
        }

        // Delete an image
        public String deleteImage(String imageDirectory, String imageName) throws IOException {
            Path imagePath = Path.of(imageDirectory, imageName);

            if (Files.exists(imagePath)) {
                Files.delete(imagePath);
                return "Success";
            } else {
                return "Failed"; // Handle missing images
            }
        }

}
