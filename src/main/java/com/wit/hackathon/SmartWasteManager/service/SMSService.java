package com.wit.hackathon.SmartWasteManager.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import com.wit.hackathon.SmartWasteManager.model.Complaint;
import com.wit.hackathon.SmartWasteManager.model.ComplaintStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Service
public class SMSService {
    public static final String ACCOUNT_SID = "";
    public static final String AUTH_TOKEN = "";

    Connection conn = DatabaseConnection.getConnection();
    Statement stmt = null;

    public void sentSMS(String compliantId, String status){
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        StringBuilder msg= new StringBuilder("");
        if("Closed".equals(status)) {
            msg.append("Hello User! your Complaint ")
                    .append(compliantId).append(" has resolved successfully !");
        }
        else if("Assigned".equals(status))
        {
                  Complaint com = getGeoCords(compliantId);
                msg.append("Hello Worker! Complaint no ").append(compliantId)
                        .append(" has assigned to you. please refer below map for directions ")
                        .append(
                                "https://www.google.com/maps/search/?api=1&query=")
            .append(com.getLatitude()).append(",").append(com.getLongitude());

        }
        Message message = Message.creator(
                        new PhoneNumber("+919028589188"),  // To
                        new PhoneNumber("+18285323613" ),  // From
                        msg.toString() )
                .create();

        System.out.println("Message SID: " + message.getSid());
    }

    public Complaint getGeoCords(String complaintId){
        Complaint complaint = new Complaint(complaintId);
        try {
            stmt = conn.createStatement();
            String sql = "SELECT latitude, longitude from Complaint where complaint_id = '" + complaintId + "'";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                complaint.setLongitude(rs.getFloat("longitude"));
                complaint.setLatitude(rs.getFloat("latitude"));
            }

        }catch (SQLException e){
            System.out.println("Complaint could not be closed");
        }
        return complaint;
    }
}
