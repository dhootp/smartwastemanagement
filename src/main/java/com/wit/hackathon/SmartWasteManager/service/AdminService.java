package com.wit.hackathon.SmartWasteManager.service;

import com.wit.hackathon.SmartWasteManager.model.Complaint;
import com.wit.hackathon.SmartWasteManager.model.ComplaintStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

@Service
public class AdminService {
    Connection conn = DatabaseConnection.getConnection();
    Statement stmt = null;

    @Autowired
    SMSService smsService;

    @Autowired
    ImageProcessingService imageProcessingService;
    public ArrayList<Complaint> getComplaints(String status){
        ArrayList<Complaint> allComplaints = new ArrayList<>();
        try {
            stmt = conn.createStatement();
            String sql = "SELECT complaint_id, area, latitude, longitude, assigned_to, image_path from Complaint where status= '" + status + "'";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Complaint complaint = new Complaint(rs.getString("complaint_id"));
                complaint.setStatus(ComplaintStatus.valueOf(status));
                complaint.setArea(rs.getString("area"));
                complaint.setLatitude(rs.getFloat("latitude"));
                complaint.setLongitude(rs.getFloat("longitude"));
                complaint.setAssignedTo(rs.getString("assigned_to"));
                String imagePath = rs.getString("image_path");
                File[] filesList = new File(imagePath).listFiles();
                if (filesList != null) {
                    byte[] image= imageProcessingService.getImage(imagePath,filesList[0].getName());
                    complaint.setImage(image);
                }
                allComplaints.add(complaint);
            }

        }catch (SQLException | IOException e){
            e.printStackTrace();
        }
        return allComplaints;
    }

    public Complaint assignComplaint(String complaintId){
        Complaint complaint = new Complaint(complaintId);
        try {
            stmt = conn.createStatement();
            String sql = "SELECT area from Complaint where complaint_id = '" + complaintId + "' and status = 'New' and assigned_to = 'NA'";
            ResultSet rs = stmt.executeQuery(sql);
            if(rs.next()) {
                String area = rs.getString("area");
                sql = "SELECT officer_name from Officer where area = '" + area + "'";
                rs = stmt.executeQuery(sql);
                if(rs.next()){
                    String officer_name = rs.getString("officer_name");
                    complaint.setStatus(ComplaintStatus.Assigned);
                    complaint.setArea(area);
                    complaint.setAssignedTo(officer_name);
                    sql = "update Complaint set status = '" + ComplaintStatus.Assigned.name() +
                            "',assigned_to = '" + officer_name + "' where complaint_id = '" + complaintId + "'";
                    stmt.execute(sql);
                    smsService.sentSMS(complaintId,"Assigned");
                }
                else {
                    System.out.println("Complaint " + complaintId + " cant be assigned as there is no officer for this area");
                    return null;
                }
            } else{
                System.out.println("Complaint " + complaintId + " should be in New");
                return  null;
            }
        }catch (SQLException e){
            e.printStackTrace();
        }
        return complaint;
    }

    public Complaint updateComplaintStatus(String complaintId, String status){
        Complaint complaint = new Complaint(complaintId);
        try {
            stmt = conn.createStatement();
            String sql = "SELECT assigned_to from Complaint where complaint_id = '" + complaintId + "'";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                complaint.setAssignedTo(rs.getString("assigned_to"));
                complaint.setStatus(ComplaintStatus.valueOf(status));
                sql = "update Complaint set status = '" + status +
                        "' where complaint_id = '" + complaintId + "'";
                stmt.execute(sql);
            }

              smsService.sentSMS(complaintId,status);

        }catch (SQLException e){
            System.out.println("Complaint could not be closed");
        }
        return complaint;
    }


}
