package com.wit.hackathon.SmartWasteManager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class SmartWasteManagerApplication {

	public static void main(String[] args) {


		SpringApplication.run(SmartWasteManagerApplication.class, args);
		System.out.println("Starting application");
	}

}
